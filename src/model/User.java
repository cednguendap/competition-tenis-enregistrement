/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * Model class for a Person.
 *
 */

/**
 *
 * @author NGUENDAP
 */
public abstract class User {

    private IntegerProperty id;
    private StringProperty nom;
    private StringProperty prenom;
    private StringProperty tel;
    private StringProperty email;
    private StringProperty mdp;
    private StringProperty sexe;
    private StringProperty photo;

    /**
     * Default constructor.
     */
 

    /**
     * Constructor with some initial data.
     * 
     * @param firstName
     * @param lastName
     */
   

    public User(int id, String nom, String prenom, String tel, String email, String mdp,String sexe) {
        this.id = new SimpleIntegerProperty(id);
        this.nom = new SimpleStringProperty(nom);
        this.prenom = new SimpleStringProperty(prenom);
        this.tel = new SimpleStringProperty(tel);
        this.email = new SimpleStringProperty(email);
        this.mdp = new SimpleStringProperty(mdp);
        this.sexe = new SimpleStringProperty(sexe);
    }
     public User(int id, String nom, String prenom, String tel, String email, String mdp,String sexe,String photo)
     {
        this(id, nom, prenom, tel, email, mdp,sexe);
        this.photo = new SimpleStringProperty(photo);
     }
    public int getId() {
        return id.get();
    }

    public String getNom() {
        return nom.get();
    }

    public String getPrenom() {
        return prenom.get();
    }

    public String getTel() {
        return tel.get();
    }
    
    public String getEmail() {
        return email.get();
    }
    
    public String getMdp() {
        return mdp.get();
    }
    
    public String getSexe() {
        return sexe.get();
    }
    
    public String getPhoto() {
        return photo.get();
    }
    
    public void setId(int id)
    {
        this.id.set(id);
    }
    
    public void setNom(String nom)
    {
        this.nom.set(nom);
    }
    public void setPrenom(String prenom)
    {
        this.prenom.set(prenom);
    }
    public void setTel(String tel)
    {
        this.tel.set(tel);
    }
    public void setEmail(String email)
    {
        this.email.set(email);
    }
    public void setMdp(String mdp)
    {
        this.mdp.set(mdp);
    }
    public void setSexe(String sexe)
    {
        this.sexe.set(sexe);
    }
    
    public void setPhoto(String photo)
    {
        this.photo.set(photo);
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public StringProperty nomProperty() {
        return nom;
    }
    
    public StringProperty prenomProperty() {
        return prenom;
    }
    
    public StringProperty telProperty() {
        return tel;
    }

    public StringProperty emailProperty() {
        return email;
    }

    public StringProperty mdpProperty() {
        return mdp;
    }
    
    public StringProperty sexeProperty() {
        return sexe;
    }
    
    public StringProperty photoProperty() {
        return photo;
    }

}

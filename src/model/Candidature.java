/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author NGUENDAP
 */
public class Candidature {
    private IntegerProperty id;
    private StringProperty dateSoumission;
    private StringProperty etatSante;
    private StringProperty motivation;
    private IntegerProperty statut;
    public static int STATUS_WAITING=0;
    public static int STATUS_VALIDATE=1;
    public static int STATUS_REJECT=-1;

    public Candidature(int id, String dateSoumission,String etatSante, String motivation) {
        this.id = new SimpleIntegerProperty(id);
        this.dateSoumission = new SimpleStringProperty(dateSoumission);
        this.etatSante = new SimpleStringProperty(etatSante);
        this.motivation = new SimpleStringProperty(motivation);
        this.statut= new SimpleIntegerProperty(Candidature.STATUS_WAITING);        
    }
    public Candidature(int id, String dateSoumission)
    {
        this.id = new SimpleIntegerProperty(id);
        this.dateSoumission = new SimpleStringProperty(dateSoumission);   
        this.statut= new SimpleIntegerProperty(Candidature.STATUS_WAITING);   
    }
    public int getId() {
        return id.get();
    }

    public String getDateSoumission() {
        return dateSoumission.get();
    }    
    
    public String getEtatSante() {
        return etatSante.get();
    }

    public String getMotivation() {
        return motivation.get();
    }

    public int getStatut() {
        return statut.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public void setDateSoumission(String dateSoumission) {
        this.dateSoumission.set(dateSoumission);
    }

    public void setEtatSante(String etatSante) {
        this.etatSante.set(etatSante);
    }

    public void setMotivation(String motivation) {
        this.motivation.set(motivation);
    }

    public void setStatut(int statut) {
        this.statut.set(statut);
    }          
    
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty dateSoumissionProperty() {
        return dateSoumission;
    }
        
    public StringProperty etatSanteProperty() {
        return etatSante;
    }

    public StringProperty motivationProperty() {
        return motivation;
    } 
    public IntegerProperty statutProperty() {
        return statut;
    } 
}

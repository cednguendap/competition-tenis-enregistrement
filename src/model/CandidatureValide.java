/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author NGUENDAP
 */
public class CandidatureValide extends Candidature
{
    private StringProperty dateValidation;
    
    public CandidatureValide(int id, String dateSoumission, String dateValidation)
    {
        super(id,dateSoumission);
        super.setStatut(Candidature.STATUS_VALIDATE);
        this.dateValidation=new SimpleStringProperty(dateValidation);
    }
    public CandidatureValide( int id, String dateSoumission,String dateValidation,String etatSante, String motivation)
    {
        super(id,dateSoumission,etatSante,motivation);
        super.setStatut(Candidature.STATUS_VALIDATE);
        this.dateValidation=new SimpleStringProperty(dateValidation);
    }
    public String getDateValidation() {
        return dateValidation.get();
    }

    public void setDateValidation(String dateValidation) {
        this.dateValidation.set(dateValidation);
    }
    
    public StringProperty dateValidationProperty() {
        return dateValidation;
    } 
    
}

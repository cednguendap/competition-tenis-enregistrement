/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author NGUENDAP
 */
public class CandidatureRejecte extends Candidature
{
    private StringProperty dateReject;
    private StringProperty raison;
    
    public CandidatureRejecte(int id, String dateSoumission, String dateReject,String raison)
    {
        super(id,dateSoumission);
        super.setStatut(Candidature.STATUS_REJECT);
        this.dateReject=new SimpleStringProperty(dateReject);
        this.raison=new SimpleStringProperty(raison);
    }
    public CandidatureRejecte( int id, String dateSoumission,String dateReject,String etatSante, String motivation, String raison)
    {
        super(id,dateSoumission,etatSante,motivation);
        super.setStatut(Candidature.STATUS_REJECT);
        this.dateReject=new SimpleStringProperty(dateReject);
        this.raison=new SimpleStringProperty(raison);
    }
    
    public String getDateReject() {
        return dateReject.get();
    }

    public String getRaison() {
        return raison.get();
    }
    
    public void setDateReject(String dateReject) {
        this.dateReject.set(dateReject);
    }

    public void setRaison(String raison) {
        this.raison.set(raison);
    }   
    
    public StringProperty dateRejectProperty() {
        return dateReject;
    }
    
    public StringProperty raisonProperty() {
        return raison;
    }
}

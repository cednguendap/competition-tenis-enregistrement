/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author NGUENDAP
 */
public class Participant extends User{

   ObjectProperty<Candidature> candidature;
    

    public Participant(int id, String nom, String prenom, String tel, String email, String mdp,String sexe) {
        super(id,nom,prenom,tel,email,mdp,sexe);        
        this.candidature=new SimpleObjectProperty(null);
    }
    public Participant(int id, String nom, String prenom, String tel, String email, String mdp,String sexe,String photo) {
        super(id,nom,prenom,tel,email,mdp,sexe,photo);        
        this.candidature=new SimpleObjectProperty(null);
    }
    public Participant(int id, String nom, String prenom, String tel, String email, String mdp,String sexe,Candidature candidature) {
        super(id,nom,prenom,tel,email,mdp,sexe);        
        this.candidature=new SimpleObjectProperty(candidature);
    }

    public Candidature getCandidature() {
        return candidature.get();
    }

    public void setCandidature(Candidature candidature) {
        this.candidature.set(candidature);
    }  

    public ObjectProperty<Candidature> candidatureProperty() {
        return candidature;
    }
    
    

   
}

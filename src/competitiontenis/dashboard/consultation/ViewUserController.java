/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.consultation;

import competitiontenis.dashboard.DashboardController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class ViewUserController implements Initializable {

    private Participant participant;
    private String type;
    private ConsultationController consultationController;
    private Parent askValidationPane;
    @FXML
    private Label labelNom;

    @FXML
    private Label labelPrenom;

    @FXML
    private Label labelSexe;

    @FXML
    private Label labelEmail;

    @FXML
    private Label labelTel;

    @FXML
    private Label labelStatut;
    
    @FXML
    private Circle zonePhoto;
    
    @FXML
    private Button btnAcceptCandidature;
    
    
    @FXML
    private Button btnConfirmValid;

    @FXML
    private Button btnAnnuler;
     
    @FXML
    private Button btnRejectCandidature;
    
    private String photoUrl="img/avatar.png";
    
    public static String ACTION_VALIDE="valide";
    public static String ACTION_REJECT="reject";
    private String action=ACTION_VALIDE;
    public void setType(String type)
    {
        this.type=type;
    }
    
    public void setAction(String action)
    {
        this.action=action;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    public void setParticipant(Participant participant)
    {
        this.participant=participant;
        labelNom.setText(participant.getNom());
        labelPrenom.setText(participant.getPrenom());
        //labelSexe.setText(participant.);
        labelEmail.setText(participant.getEmail());
        labelTel.setText(participant.getTel());
        Image image=new Image(DashboardController.class.getResource(photoUrl).toExternalForm());     
        zonePhoto.setFill(new ImagePattern(image));
    }
    
    public void setConsultationController(ConsultationController consultationController)
    {
        this.consultationController=consultationController;
    }
    
    @FXML
    void handleRejectCandidature(ActionEvent event) {
        action=ACTION_REJECT;
        showButtonAskValidation();
        askValidationPane=consultationController.showUIAskRaison(type);
    }

    @FXML
    void handleValidCandidature(ActionEvent event) {
        action=ACTION_VALIDE;
        showButtonAskValidation();
        consultationController.showUIAskValidation(type);
    }
    @FXML
    void handleBackTo(ActionEvent event) {
        action="";
        hideButtonAskValidation();
        consultationController.backToShowUIParticipant(type);
    }
    
    @FXML
    void handleChoixVerifierCandidature(ActionEvent event)
    {
        if(action==ACTION_VALIDE) consultationController.acceptCandidatureParticipant(participant,type);
        else if(action==ACTION_REJECT) 
        {
            String raison=((TextArea)askValidationPane.lookup("#textRaison")).getText();
            if(raison.equals("") || raison.equals(" ")) askValidationPane.lookup("#textMessageErreur").setVisible(true);
            else  consultationController.rejectCandidatureParticipant(participant,type,raison);
        }
    }
    
    public void showButtonAskValidation()
    {
        btnAcceptCandidature.setVisible(false);
        btnRejectCandidature.setVisible(false);
        btnConfirmValid.setVisible(true);
        btnAnnuler.setVisible(true);
    }
    public void hideButtonAskValidation()
    {
       btnAcceptCandidature.setVisible(true);
       btnRejectCandidature.setVisible(true);    
       btnConfirmValid.setVisible(false);
       btnAnnuler.setVisible(false);
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.consultation;

import competitiontenis.auth.AuthController;
import competitiontenis.dashboard.DashboardController;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import model.Candidature;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class CustomProfileCellController extends ListCell<Participant> {

    private Parent viewContener=null;
    private String photoUrl="img/avatar.png";
    @FXML Circle photoZone;
    
    @FXML 
    private Label labelNom;
    
    @FXML 
    private Label labelStatut;
    
    
    @Override
    public void updateItem(Participant participantItem, boolean empty)
    {
        super.updateItem(participantItem, empty);
        Image image=new Image(DashboardController.class.getResource(photoUrl).toExternalForm());     
        photoZone.setFill(new ImagePattern(image));
        
        if(participantItem!=null && !empty)
        {
            labelNom.setText(participantItem.getNom()+' '+participantItem.getPrenom());
            if(participantItem.getCandidature()!=null)
            {
                if(participantItem.getCandidature().getStatut()==Candidature.STATUS_REJECT)
                {
                    labelStatut.setText("Rejeté");
                }
                else if(participantItem.getCandidature().getStatut()==Candidature.STATUS_WAITING)
                {
                    labelStatut.setText("En attente");
                }
                else if(participantItem.getCandidature().getStatut()==Candidature.STATUS_VALIDATE)
                {
                    labelStatut.setText("Validé");
                }    
                else
                {
                    labelStatut.setVisible(false);
                }
            }
            else labelStatut.setVisible(false);
            setGraphic(viewContener);
        }
        else
        {
            setGraphic(null);
        }
        setText(null);       
    }
    public CustomProfileCellController()
    {
        super();
    }
    public void setViewConteneur(Parent viewConteneur)
    {
        this.viewContener=viewConteneur;
    }
    public Parent getViewConteneur()
    {
        return viewContener;
    }
    public static CustomProfileCellController load()
    {
        CustomProfileCellController customProfileCellControlller=null;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ConsultationController.class.getResource("customProfileCell.fxml"));   
            AnchorPane customProfilView=(AnchorPane)loader.load();
            customProfileCellControlller=loader.getController();
            customProfileCellControlller.setViewConteneur(customProfilView);
            
            //((SigninController)loader.getController()).setAuthController(this);
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return customProfileCellControlller;
    }
}

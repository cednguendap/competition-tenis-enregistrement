/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.consultation;

import competitiontenis.databaseaccess.ObservableDataStorage;
import competitiontenis.databaseaccess.Requete;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.Candidature;
import model.CandidatureRejecte;
import model.CandidatureValide;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class ConsultationController implements Initializable{

    Requete requete;
    
    @FXML private Pane participantAllInformationPane;
    @FXML private Pane participantALireInformationPane;
    @FXML private Pane participantValiderInformationPane;
    @FXML private Pane participantRejeterInformationPane;
    
    @FXML private TabPane tabPaneConsultation;
    @FXML private Tab labelTousTab;
    @FXML private Tab labelALireTab;
    @FXML private Tab labelValiderTab;
    @FXML private Tab labelRejeterTab;
    
    @FXML ListView<Participant> listViewAllCandidatureParticipant;
    @FXML ListView<Participant> listViewWaintingCandidatureParticipant;
    @FXML ListView<Participant> listViewValideCandidatureParticipant;
    @FXML ListView<Participant> listViewRejeteCandidatureParticipant;
    
    ObservableDataStorage dataStorage=ObservableDataStorage.getInstance();
    
    /**
     * Initializes the controller class.
     */  
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        //spécification des observable list a utilisé par les listViews
        listViewAllCandidatureParticipant.setItems(dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_ALL));
        listViewWaintingCandidatureParticipant.setItems(dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_WAIT));
        listViewValideCandidatureParticipant.setItems(dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_VALID));
        listViewRejeteCandidatureParticipant.setItems(dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_REJECT));
        
        //ajout des factory pour la spécification de la facon dont le listeview affiche les différentes items(participant)
        listViewAllCandidatureParticipant.setCellFactory(listView->CustomProfileCellController.load());
        listViewWaintingCandidatureParticipant.setCellFactory(listView->CustomProfileCellController.load());
        listViewValideCandidatureParticipant.setCellFactory(listView->CustomProfileCellController.load());
        listViewRejeteCandidatureParticipant.setCellFactory(listView->CustomProfileCellController.load());
             
        //ajout des listener pour la détection des ajoutes et des suppréssions dans les observables list pour la 
        //mise a jour du text dans les titres des tabs
        dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_ALL).addListener((ListChangeListener.Change<? extends Participant> participant) -> updateTabLabel(participant,"all"));
        dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_WAIT).addListener((ListChangeListener.Change<? extends Participant> participant) -> updateTabLabel(participant,"wait"));
        dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_VALID).addListener((ListChangeListener.Change<? extends Participant> participant) -> updateTabLabel(participant,"valid"));
        dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_REJECT).addListener((ListChangeListener.Change<? extends Participant> participant) -> updateTabLabel(participant,"reject"));
                
        //gestion de la selection d'un participant pour l'affichage des détails du compte
        listViewAllCandidatureParticipant.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable,oldValue,newValue)->showParticipantDetails(newValue,"all"));
        listViewWaintingCandidatureParticipant.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable,oldValue,newValue)->showParticipantDetails(newValue,"wait"));
        listViewValideCandidatureParticipant.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable,oldValue,newValue)->showParticipantDetails(newValue,"valid"));
        listViewRejeteCandidatureParticipant.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable,oldValue,newValue)->showParticipantDetails(newValue,"reject"));
        
        tabPaneConsultation.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            showFristParticipant(getStringLabelOfTab(newValue.intValue()));
        }); 
        dataStorage.initializeData();
        tabPaneConsultation.getSelectionModel().selectFirst();
        showFristParticipant("all");
    }    
    private int getNumberOfTab(String type)
    {
        if(null!=type) 
            switch (type) {
                case "all":
                    return 0;
                case "wait":
                    return 1;
                case "valid":
                    return 2;
                case "reject":
                    return 3;
            }
        return -1;
    }
    private String getStringLabelOfTab(int value)
    {
        switch(value)
        {
            case 0: 
                return "all";
            case 1: 
                return "wait";
            case 2: 
                return "valid";
            case 3: 
                return "reject";           
        }
        return "";
    }
    private void updateTabLabel(ListChangeListener.Change<? extends Participant> participant,String type)
    {
        while(participant.next())
        {
            if(participant.wasRemoved() || participant.wasAdded())
            {
                if(null!=type) switch (type) {
                    case "all":
                        labelTousTab.setText("("+dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_ALL).size()+") Tous");
                        break;
                    case "wait":
                        labelALireTab.setText("("+dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_WAIT).size()+") A lire");
                        break;
                    case "valid":
                        labelValiderTab.setText("("+dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_VALID).size()+") Validés");
                        break;
                    case "reject":
                        labelRejeterTab.setText("("+dataStorage.getObservableListBy(ObservableDataStorage.OBSERVABLE_TYPE_REJECT).size()+") Rejetés");
                        break;
                }
            }
        }
    }
    
    private ListView<Participant> getListViewParticipantBy(String type)
    {
        if(null!=type) 
            switch (type) 
            {
                case "all":
                    return listViewAllCandidatureParticipant;
                case "wait":
                    return listViewWaintingCandidatureParticipant;
                case "valid":
                    return listViewValideCandidatureParticipant;
                case "reject":
                    return listViewRejeteCandidatureParticipant;
            }   
        return null;
    }
    
    private Pane getParticipantTabPaneBy(String type)
    {
        if(null!=type) 
            switch (type) 
            {
                case "all":
                    return participantAllInformationPane;
                case "wait":
                    return  participantALireInformationPane;
                case "valid":
                    return participantValiderInformationPane;
                case "reject":
                    return participantRejeterInformationPane;
            }
        return null;
    }
    private Tab getParticipantTabBy(String type)
    {
        if(null!=type) 
            switch (type) 
            {
                case "all":
                    return labelTousTab;
                case "wait":
                    return  labelALireTab;
                case "valid":
                    return labelValiderTab;
                case "reject":
                    return labelRejeterTab;
            }
        return null;
    }
    private void showParticipantDetails(Participant participant,String type)
    {      
        try {
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("viewUser.fxml"));            
            VBox viewUserZone=(VBox)loader.load();      
            getParticipantTabPaneBy(type).getChildren().clear();
            if(participant==null) return;
            //-------------a activé quand module pret------------------------------------------
            if(participant.getCandidature()==null 
                    || participant.getCandidature().getStatut()!=Candidature.STATUS_WAITING )
            {
                ((SplitPane)viewUserZone.lookup("#btnActionZone")).setVisible(false);
            }            
            
            getParticipantTabPaneBy(type).getChildren().add(viewUserZone);           
            
           
            ViewUserController viewUserController= loader.getController();
            
            viewUserController.hideButtonAskValidation();
            viewUserController.setType(type);
            viewUserController.setConsultationController(this);
            viewUserController.setParticipant(participant);
        } catch (IOException ex) {
            Logger.getLogger(ConsultationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void showUIAskValidation(String type)
    {
        try
        {
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("validDecisionToCandidatureParticipant.fxml"));
            ((HBox)getParticipantTabPaneBy(type).lookup("#zoneDetailInfos")).getChildren().clear();     
            
            VBox askValidationPane=(VBox)loader.load();
            
            ((ValidDecisionToCandidatureParticipant)loader.getController()).setConsultationController(this);
            ((ValidDecisionToCandidatureParticipant)loader.getController()).setType(type);
            askValidationPane.getChildren().remove(1);
            ((HBox)getParticipantTabPaneBy(type).lookup("#zoneDetailInfos")).getChildren().add(askValidationPane);
           
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ConsultationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Parent showUIAskRaison(String type)
    {
        VBox askValidationPane=null;
        try {
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("validDecisionToCandidatureParticipant.fxml"));
            ((HBox)getParticipantTabPaneBy(type).lookup("#zoneDetailInfos")).getChildren().clear();     
            
            askValidationPane=(VBox)loader.load();                       
            askValidationPane.lookup("#textMessageErreur").setVisible(false);
            ((ValidDecisionToCandidatureParticipant)loader.getController()).setConsultationController(this);
            ((ValidDecisionToCandidatureParticipant)loader.getController()).setType(type);
            askValidationPane.getChildren().remove(0);
            ((HBox)getParticipantTabPaneBy(type).lookup("#zoneDetailInfos")).getChildren().add(askValidationPane);
            
        } catch (IOException ex) 
        {
            Logger.getLogger(ConsultationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return askValidationPane;
    }
    
    public void backToShowUIParticipant(String type)
    {
        Participant participant=getListViewParticipantBy(type).getSelectionModel().getSelectedItem();             
        showParticipantDetails(participant, type);
    }
    public void acceptCandidatureParticipant(Participant participant,String type)
    {
        CandidatureValide candidatureValide=new CandidatureValide(participant.getCandidature().getId(),participant.getCandidature().getDateSoumission(),"");
        participant.setCandidature(candidatureValide);
        showFristParticipant(type);
    }
    public void rejectCandidatureParticipant(Participant participant,String type,String raison)
    {
        CandidatureRejecte candidatureValide=new CandidatureRejecte(participant.getCandidature().getId(),participant.getCandidature().getDateSoumission(),"",raison);
        participant.setCandidature(candidatureValide);
        showFristParticipant(type);
    }
    public void showFristParticipant(String type)
    {
        System.out.println("Tab Type "+type+" nombre "+dataStorage.getObservableListBy(type).size());        
        if(dataStorage.getObservableListBy(type).size()>0)
        {
            tabPaneConsultation.getSelectionModel().select(getNumberOfTab(type));
            getListViewParticipantBy(type).scrollTo(0);            
            getListViewParticipantBy(type).getSelectionModel().select(0);
            backToShowUIParticipant(type);
        }
        else
        {
            
            //((HBox)getParticipantTabPaneBy(type).lookup("#zoneDetailInfos")).getChildren().clear();
        }
    }
}

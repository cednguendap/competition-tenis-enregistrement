/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard;

import competitiontenis.auth.AuthController;
import competitiontenis.dashboard.consultation.ConsultationController;
import competitiontenis.dashboard.macandidature.CreerCandidatureController;
import competitiontenis.dashboard.macandidature.MaCandidatureController;
import competitiontenis.databaseaccess.ObservableDataStorage;
import competitiontenis.databaseaccess.Requete;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import model.Participant;
import model.User;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class DashboardController implements Initializable
{
    BorderPane rootLayout;
    private String photoUrl="img/avatar.png";
        
    @FXML private Circle photoProfilZone;      
    @FXML private Label labelNomCurrentUser;
    @FXML private Button btnConstulterDonnees;
    @FXML private Button btnGestionCandidature;
    @FXML private VBox menuVbox;
    ObservableDataStorage daoObservable=ObservableDataStorage.getInstance();
    
    public void setRootLayout(BorderPane rootLayout) {
        this.rootLayout = rootLayout;
    } 
    public void showUIHome()
    {
        /*try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SigninController.class.getResource("signin.fxml"));            
            Pane rightPane=(Pane)rootLayout.getRight();
            rightPane.getChildren().clear();
            rightPane.getChildren().add((AnchorPane)loader.load());
            //((SigninController)loader.getController()).setAuthController(this);
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        }  */
    }
    public void showConsulterDonneeUI()
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ConsultationController.class.getResource("consultation.fxml")); 
            rootLayout.setCenter((TabPane)loader.load());
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    public void showMaCandidatureUI()
    {
        MaCandidatureController maCandidatureController=new MaCandidatureController();
        maCandidatureController.setRootLayout(rootLayout);
        maCandidatureController.showCustomUI();
    }
    public void showHelpUI()
    {
        
    }
    public void showAProposUI()
    {
        
    }
    public void setProfilPhotoContent()
    {
        Image image=new Image(getClass().getResource(photoUrl).toExternalForm());
        photoProfilZone.setFill(new ImagePattern(image));
    }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        User currentUser=daoObservable.getCurrentUser();
        labelNomCurrentUser.setText(currentUser.getNom());
        if("Participant".equals(currentUser.getClass().getSimpleName()) && ((Participant)currentUser).getCandidature()==null)
             menuVbox.getChildren().remove(btnConstulterDonnees);
        if("Administrateur".equals(currentUser.getClass().getSimpleName()))
            menuVbox.getChildren().remove(btnGestionCandidature);
    }
    
}

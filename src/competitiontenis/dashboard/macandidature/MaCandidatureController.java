/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.macandidature;

import competitiontenis.auth.AuthController;
import competitiontenis.databaseaccess.ObservableDataStorage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import model.Participant;

/**
 *
 * @author NGUENDAP
 */
public class MaCandidatureController {
    
    private BorderPane rootLayout;
    private ObservableDataStorage dataStorage=ObservableDataStorage.getInstance();
    private Participant currentUser=(Participant)dataStorage.getCurrentUser();
    public void setRootLayout(BorderPane rootLayout)
    {
        this.rootLayout=rootLayout;
    }
    
    public void showUICreerCandidature()
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CreerCandidatureController.class.getResource("creerCandidature.fxml")); 
            BorderPane contentCreerCandidature=(BorderPane)loader.load();
            CreerCandidatureController creerCandidatureController=(CreerCandidatureController)loader.getController();
            rootLayout.setCenter(contentCreerCandidature);            
            creerCandidatureController.setRootLayout(contentCreerCandidature);
            creerCandidatureController.showFormulaireInformationPersonnel();
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void showCustomUI()
    {
        //ici la décision d'afficher une ui en fonction de ce qu'on la 
        showUICreerCandidature();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.macandidature;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;

/**
 *
 * @author NGUENDAP
 */
public class ConfirmCreationCandidatureController implements Initializable{
    
    private String content="<body style=\"background-color: rgb(45,44,49);color: rgba(255,255,255,0.8);;font-family:Arial;line-height:1.5;\">Merci de l'attention que vous accordé a notre compétition. "
            + "<br/> Veuillez vérifier que  vous avez saisi tout les informations en vérifiant "
            + "les consignes suivantes:"
            + "<ul>"
            + "<li>Vérifier vos informations personnelles</li>"
            + "<li>Rélisez votre motivation pour vos assurer de votre élligibilité au prés des juré</li>"
            + "</ul><p>Vérifier de temps a autres le contenus de l'onglet candidature pour voir l'état"
            + " de votre candidature</p>Merci de votre participation <br/><div style=\"margin-top:20px;\"><center><u>L'équipe de tenis de l'université "
            + "des montagnes</u></center></div></body>";
    @FXML  private WebView webViewContent; 

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        webViewContent.getEngine().loadContent(content);
    }
    
    
}

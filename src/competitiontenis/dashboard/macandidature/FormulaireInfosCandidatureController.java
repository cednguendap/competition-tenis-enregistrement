/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.macandidature;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.Candidature;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class FormulaireInfosCandidatureController {

    @FXML
    private TextField etatSanteInput;

    @FXML
    private TextArea motivationInput;
    
    public Candidature getCandidature()
    {
        return new Candidature(0,new SimpleDateFormat("dd/MM/yyyy").format(new Date()),etatSanteInput.getText(),motivationInput.getText());
    }
    
}

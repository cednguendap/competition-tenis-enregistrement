/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.macandidature;

import competitiontenis.databaseaccess.ObservableDataStorage;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class FormulaireInfoPersonCandidatureController implements Initializable {

    @FXML
    private TextField nomInput;

    @FXML
    private TextField prenomInput;

    @FXML
    private TextField telInput;

    @FXML
    private TextField emailInput;

    @FXML
    private ChoiceBox<String> sexeInput;
    private Participant participant=(Participant)ObservableDataStorage.getInstance().getCurrentUser();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sexeInput.getItems().add("Homme");
        sexeInput.getItems().add("Femme");
        nomInput.setText(participant.getNom());
        prenomInput.setText(participant.getPrenom());
        telInput.setText(participant.getTel());
        emailInput.setText(participant.getEmail());
        sexeInput.setValue((participant.getSexe()=="M" || participant.getSexe()=="")?"Homme":"Femme");
    }    
    
    public Participant getInformationUser()
    {
        Participant participant=null;
        String nom=nomInput.getText();
        String prenom=prenomInput.getText();
        String tel=telInput.getText();
        String email=emailInput.getText();
        String sexe=sexeInput.getValue();
        return new Participant(0,nom,prenom,tel,email,"","");
    }
}

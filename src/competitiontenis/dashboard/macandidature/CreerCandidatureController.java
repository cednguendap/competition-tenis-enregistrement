/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.dashboard.macandidature;

import competitiontenis.auth.AuthController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class CreerCandidatureController implements Initializable {

    private BorderPane rootLayout;
    @FXML  private Button btnActionPrecedent;

    @FXML  private Button btnActionSuivant;

    @FXML  private Button btnActionTerminer;
    
    
    
    private int pagenbre=0;
    
    private AnchorPane formulaireInfosPersonnelPane;
    private FormulaireInfoPersonCandidatureController formulaireInfoPersonCandidatureController;
    
    private AnchorPane formulaireInfosCandidaturePane;
    private FormulaireInfosCandidatureController formulaireInfosCandidatureController; 
    
    private AnchorPane pageEndSousmissionCandidaturePane;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CreerCandidatureController.class.getResource("formulaireInfoPersonCandidature.fxml")); 
            formulaireInfosPersonnelPane=(AnchorPane)loader.load();
            formulaireInfoPersonCandidatureController=loader.getController();
            
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(CreerCandidatureController.class.getResource("formulaireInfosCandidature.fxml"));
            formulaireInfosCandidaturePane=(AnchorPane)loader2.load();
            formulaireInfosCandidatureController=loader2.getController();
            
            FXMLLoader loader3 = new FXMLLoader();
            loader3.setLocation(CreerCandidatureController.class.getResource("confirmerCreationCandidature.fxml"));
            pageEndSousmissionCandidaturePane=(AnchorPane)loader3.load();
            
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    public void setRootLayout(BorderPane rootLayout)
    {
        this.rootLayout=rootLayout;
    }
    public void showFormulaireInformationPersonnel()
    {
        rootLayout.setCenter(formulaireInfosPersonnelPane);
        disableButtonForFirstPage();
    }   
    public void showFormulaireInformationCandidature()
    {
        rootLayout.setCenter(formulaireInfosCandidaturePane);
        disableButtonForSecondPage();
    }
    public void showPageConfirmationCandidature()
    {
        rootLayout.setCenter(pageEndSousmissionCandidaturePane);
        disableButtonForEndPage();
    }
    @FXML
    void handleActionPrecedent(ActionEvent event) 
    {
        pagenbre--;
        if(pagenbre==0) showFormulaireInformationPersonnel();
        if(pagenbre==1) showFormulaireInformationCandidature();
    }

    @FXML
    void handleActionSuivant(ActionEvent event) {
        pagenbre++;
        if(pagenbre==1) showFormulaireInformationCandidature();
        if(pagenbre==2) showPageConfirmationCandidature();
    }

    @FXML
    void handleActionTerminer(ActionEvent event) {
        Participant participant=formulaireInfoPersonCandidatureController.getInformationUser();
    }
    
    public void disableButtonForFirstPage()
    {
        btnActionPrecedent.setDisable(true);
        btnActionSuivant.setDisable(false);
        btnActionTerminer.setDisable(true);
    }
    public void disableButtonForSecondPage()
    {
        btnActionPrecedent.setDisable(false);
        btnActionSuivant.setDisable(false);
        btnActionTerminer.setDisable(true);
    }
    public void disableButtonForEndPage()
    {
        btnActionPrecedent.setDisable(false);
        btnActionSuivant.setDisable(true);
        btnActionTerminer.setDisable(false);
    }

    
}

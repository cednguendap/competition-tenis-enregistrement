/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.auth.signin;

import competitiontenis.auth.AuthController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Participant;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class SigninController implements Initializable {
    
    AuthController authController;

    @FXML
    private TextField nomInput;

    @FXML
    private TextField prenomInput;

    @FXML
    private TextField emailInput;

    @FXML
    private TextField mdpInput;

    @FXML
    private Button submitButton;
    
    @FXML private Label labelErreur;
    
    @FXML
    void onSubmit(ActionEvent event) {
        authController.signinAction(new Participant(0,nomInput.getText(),prenomInput.getText(),"0",emailInput.getText()
            ,mdpInput.getText(),"",""));
    }
    
    @FXML
    void onAlreadyHaveAccount(ActionEvent event)
    {
        authController.showSignupUI();
    }
    
    public void setAuthController(AuthController authController) {
        this.authController = authController;
    }
    public void setVisibleErrorMessage(Boolean visibility)
    {
        labelErreur.setVisible(visibility);
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labelErreur.setVisible(false);
    }    
    
}

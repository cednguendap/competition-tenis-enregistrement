/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.auth.signup;

import competitiontenis.auth.AuthController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class SignupController implements Initializable {

    AuthController authController;
    
    @FXML
    private TextField emailInput;

    @FXML
    private TextField passwordInput;

    @FXML
    private Button submitButton;

    @FXML private Label labelErreur;
    @FXML
    void onSubmit(ActionEvent event) {
        authController.signupAction(emailInput.getText(), passwordInput.getText());        
    }
    
    @FXML
    void onAlreadyHaveAccount(ActionEvent event)
    {
        authController.showSigninUI();
    }
    
    public void setAuthController(AuthController authController) {
        this.authController = authController;
    }    
    public void setVisibleErrorMessage(Boolean visibility)
    {
        labelErreur.setVisible(visibility);
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labelErreur.setVisible(false);
    }    
    
}

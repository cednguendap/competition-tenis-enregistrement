/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.auth;

import competitiontenis.auth.signin.SigninController;
import competitiontenis.auth.signup.SignupController;
import competitiontenis.dashboard.DashboardController;
import competitiontenis.databaseaccess.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Participant;
import model.User;

/**
 * FXML Controller class
 *
 * @author NGUENDAP
 */
public class AuthController{

    
    Stage stage;
    BorderPane rootLayout;
    Requete requete=null;
    double x,y;
    @FXML private Button closeWindowButton;
    ObservableDataStorage daoObservable=ObservableDataStorage.getInstance();
    
    private AnchorPane signupPane;
    private SignupController signupController;
    
    private AnchorPane signinPane;
    private SigninController signinController;
    
    public AuthController(Stage stage) {
        this.stage = stage;
    }

    public void setRootLayout(BorderPane rootLayout) {
        this.rootLayout = rootLayout;
    } 
    
    /**
     * Initializes the controller class.
     */
    @FXML      
    public void initialize()
    {
        requete=DataAccess.connexion("localhost", 3306,"tenniscompetition", "root", "");
        daoObservable.setRequete(requete);
        
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SignupController.class.getResource("signup.fxml"));
            signupPane=(AnchorPane)loader.load();
            signupController=(SignupController)loader.getController();
            signupController.setAuthController(this);
            
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(SigninController.class.getResource("signin.fxml"));       
            signinPane=(AnchorPane)loader2.load();
            signinController=(SigninController)loader2.getController();
            signinController.setAuthController(this);
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
            //ex.printStackTrace();
        }
        
    }
    
    @FXML
    public void closeWindowsButton()
    {
        Platform.exit();
    }
    
    @FXML
    void onMouseDragged(MouseEvent event) {
        Stage stage=(Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX()-x);
        stage.setY(event.getScreenY()-y);
    }

    @FXML
    void onMousePressed(MouseEvent event) {
        x=event.getSceneX();
        y=event.getSceneY();
    }
    
    @FXML
    void onMinimize(MouseEvent event)
    {
        Stage stage=(Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }   
    
    public void showSignupUI()
    {
        Pane rightPane=(Pane)rootLayout.getRight();
        rightPane.getChildren().clear();
        rightPane.getChildren().add(signupPane);
    }
    
    public void showSigninUI()
    {
        Pane rightPane=(Pane)rootLayout.getRight();
        rightPane.getChildren().clear();
        rightPane.getChildren().add(signinPane);          
    }
    
    public void signupAction(String email,String mdp)
    {
        User currentUser=daoObservable.getDaoUser().connexion(email, mdp);
        if(currentUser==null) signupController.setVisibleErrorMessage(true);
        else {
            signupController.setVisibleErrorMessage(false);
            daoObservable.setCurrentUser(currentUser);
            showDashboardUI();
        }
    }
    public void signinAction(Participant participant)
    {
        if(daoObservable.getDaoUser().save(participant).getStatut()==ResultRequest.OK)
        {
            daoObservable.setCurrentUser(daoObservable.getDaoUser().connexion(participant.getEmail(), participant.getMdp()));
            showDashboardUI();
        }
        else signinController.setVisibleErrorMessage(true);
    }
    public void showDashboardUI()
    {
       
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(DashboardController.class.getResource("dashboard.fxml"));
            BorderPane root = (BorderPane)loader.load();            
            stage.setScene(new Scene(root));
            DashboardController dashboardController=loader.getController();
            dashboardController.setRootLayout(root);
            //dashboardController.showConsulterDonneeUI();
        } catch (IOException ex) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public Requete getRequete()
    {
        return requete;
    }
    
}

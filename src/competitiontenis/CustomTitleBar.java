/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 *
 * @author NGUENDAP
 */
public class CustomTitleBar {

    public double x;
    public double y;
    
    @FXML
    public void closeWindowsButton()
    {
        Platform.exit();
    }
    
    @FXML
    void onMouseDragged(MouseEvent event) {
        Stage stage=(Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX()-x);
        stage.setY(event.getScreenY()-y);
    }

    @FXML
    void onMousePressed(MouseEvent event) {
        x=event.getSceneX();
        y=event.getSceneY();
    }
    
    @FXML
    void onMinimize(MouseEvent event)
    {
        Stage stage=(Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.interfaces;

import competitiontenis.databaseaccess.Requete;
import competitiontenis.databaseaccess.ResultRequest;

/**
 *
 * @author NGUENDAP
 */
public abstract class DAOController<T> 
{
    protected Requete requete;
    
    public void setRequete(Requete requete)
    {
        this.requete=requete;
        System.out.println("Dans le setRequetehbg");
        System.out.println(requete);
    }
    public abstract ResultRequest save(T data);
    public abstract ResultRequest update(T data);
    public abstract ResultRequest delete(T data);
    public abstract ResultRequest fetchAll();
    public abstract ResultRequest fetchOne(T data);
}

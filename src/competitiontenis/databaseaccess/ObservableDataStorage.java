/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.databaseaccess;

import competitiontenis.databaseaccess.ResultRequest;
import java.util.ArrayList;
import java.util.Date;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.util.Callback;
import model.Candidature;
import model.Participant;
import model.User;

/**
 *
 * @author NGUENDAP
 */
public class ObservableDataStorage 
{   
    private static volatile ObservableDataStorage instance = null;
            
    Callback<Participant,Observable[]> participantExtractor=new Callback<Participant, Observable[]>(){
        @Override
        public Observable[] call(Participant param) {
            return new Observable[]{ param.candidatureProperty()};
        }    
    };

    private ObservableList<Participant> allCandidatureParticipant = FXCollections.observableArrayList(participantExtractor);
    private ObservableList<Participant> waitingCandidatureParticipant = FXCollections.observableArrayList(participantExtractor);
    private ObservableList<Participant> valideCandidatureParticipant = FXCollections.observableArrayList();
    private ObservableList<Participant> rejecteCandidatureParticipant = FXCollections.observableArrayList();
    
    public static String OBSERVABLE_TYPE_ALL="all";
    public static String OBSERVABLE_TYPE_WAIT="wait";
    public static String OBSERVABLE_TYPE_VALID="valid";
    public static String OBSERVABLE_TYPE_REJECT="reject";
    
    private DAOCandidatureController daoCandidature=new DAOCandidatureController();
    private DAOUserController daoUser=new DAOUserController(daoCandidature);
    private Requete requete;
    
    private User currentUser;

    private ObservableDataStorage()
    {
        super();
        initializeListener();        
    }
    public void initializeListener()
    {
        allCandidatureParticipant.addListener((ListChangeListener.Change<? extends Participant> participant) -> addAllTableDataListener(participant));
        allCandidatureParticipant.addListener((ListChangeListener.Change<? extends Participant> participant) -> updateTableDataListener(participant,OBSERVABLE_TYPE_ALL));    
        
    }
    public void initializeData()
    {        
        /*allCandidatureParticipant.add(new Participant(1,"Cedric","Nguendap","6 76 66 99 33","cednguendap@gmail.com","mdp","",new Candidature(1,new Date())));
        allCandidatureParticipant.add(new Participant(2,"Flambel","Sanou","6 76 66 99 33","flambelsanou@gmail.com","mdp","",new Candidature(2,new Date())));
        allCandidatureParticipant.add(new Participant(3,"Dylan","Takoubo","6 76 66 99 33","dylanttd@gmail.com","mdp",""));
        allCandidatureParticipant.add(new Participant(4,"Ivan","Flamel","6 76 66 99 33","Ivan@gmail.com","mdp",""));
        allCandidatureParticipant.add(new Participant(5,"Ngounou","Delors","6 76 66 99 33","delors@gmail.com","mdp",""));*/
        ResultRequest result=daoUser.fetchAll();
        
        if(result.getStatut()==ResultRequest.OK)
        {
            ((ArrayList)result.getResult()).forEach(( item) -> {
                Participant participant=(Participant)item;
                allCandidatureParticipant.add(participant);
                if(participant.getCandidature()!=null)
                {
                    if(participant.getCandidature().getStatut()==Candidature.STATUS_WAITING)
                    {
                        waitingCandidatureParticipant.add(participant);
                    }
                    else if(participant.getCandidature().getStatut()==Candidature.STATUS_VALIDATE)
                    {
                       valideCandidatureParticipant.add(participant);
                    }
                    else 
                    {
                        rejecteCandidatureParticipant.add(participant);
                    } 
                }
            });
        }
        else
        {
            System.out.println(result.getDescriptionResult());
        }
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public DAOCandidatureController getDaoCandidature() {
        return daoCandidature;
    }

    public DAOUserController getDaoUser() {
        return daoUser;
    }

    public void setDaoCandidature(DAOCandidatureController daoCandidature) {
        this.daoCandidature = daoCandidature;
    }

    public void setDaoUser(DAOUserController daoUser) {
        this.daoUser = daoUser;
    }

    public void setRequete(Requete requete) {
        this.requete = requete;
        daoCandidature.setRequete(requete);
        daoUser.setRequete(requete);
    }
    
    public ObservableList<Participant> getObservableListBy(String type)
    {
        if(null!=type) 
            if(type.equals(OBSERVABLE_TYPE_ALL)) return allCandidatureParticipant;
            if(type.equals(OBSERVABLE_TYPE_WAIT)) return waitingCandidatureParticipant;
            if(type.equals(OBSERVABLE_TYPE_VALID)) return valideCandidatureParticipant;
            if(type.equals(OBSERVABLE_TYPE_REJECT)) return rejecteCandidatureParticipant;
        return null;
    } 
    private void addAllTableDataListener(ListChangeListener.Change<? extends Participant> participant)
    {        
        while(participant.next())
        {
            if(participant.wasAdded())
            {
                participant
                .getAddedSubList()
                .stream()
                .filter((candidat) -> (candidat.getCandidature()!=null))                
                .forEachOrdered((candidat) -> 
                {
                    if(candidat.getCandidature().getStatut()==Candidature.STATUS_WAITING)
                        waitingCandidatureParticipant.add(candidat);
                    else if(candidat.getCandidature().getStatut()==Candidature.STATUS_VALIDATE)
                        valideCandidatureParticipant.add(candidat);
                    else if(candidat.getCandidature().getStatut()==Candidature.STATUS_REJECT)
                        rejecteCandidatureParticipant.add(candidat);
                });
            }
        }
    }
    private void updateTableDataListener(ListChangeListener.Change<? extends Participant> participant,String type)
    {
        while(participant.next())
        {
            if(participant.wasUpdated())
            {
                for(int i=participant.getFrom();i<participant.getTo();i++)
                {
                    Participant candidat =participant.getList().get(i);
                    waitingCandidatureParticipant.removeIf(particip->particip.getId()==candidat.getId());
                    if(candidat.getCandidature().getStatut()==Candidature.STATUS_VALIDATE)
                        valideCandidatureParticipant.add(candidat);
                    else if(candidat.getCandidature().getStatut()==Candidature.STATUS_REJECT)
                        rejecteCandidatureParticipant.add(candidat);               
                }               
            }
        }
    }
    public int findPosition(Participant participant, String type)
    {
        for(int i=0;i<getObservableListBy(type).size();i++)
        {
            if(getObservableListBy(type).get(i).getId()==participant.getId()) return i;
        }
        return -1;
    }
     public final static ObservableDataStorage getInstance() {
         if (ObservableDataStorage.instance == null) {
            synchronized(ObservableDataStorage.class) {
              if (ObservableDataStorage.instance == null) {
                ObservableDataStorage.instance = new ObservableDataStorage();
              }
            }
         }
         return ObservableDataStorage.instance;
     }
}


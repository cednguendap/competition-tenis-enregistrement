/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.databaseaccess;

import competitiontenis.interfaces.DAOController;
import static java.lang.Integer.parseInt;
import java.util.Date;
import java.util.Map;
import model.Candidature;
import model.CandidatureRejecte;
import model.CandidatureValide;
import model.Participant;

/**
 *
 * @author NGUENDAP
 */
public class DAOCandidatureController extends DAOController<Candidature> {
    
    @Override
    public ResultRequest save(Candidature data) {
        String dateValidation="''";
        String dateRejecte="''";
        String type="'0'";
        String raison="''";
        
        if(data.getClass().getSimpleName()=="CandidatureRejecte")
        {
            dateRejecte="'"+((CandidatureRejecte)data).getDateReject()+"'";
            type="'2'";
            raison="'"+((CandidatureRejecte)data).getRaison()+"'";
        }
        else if(data.getClass().getSimpleName()=="CandidatureValide") 
        {
            dateValidation="'"+((CandidatureValide)data).getDateValidation()+"'";
            type="'1'";
        }
        return requete.add("INSERT INTO candidature (etat_sante,moditivation,date_soumission,date_rejet,"
                        + "date_validation,raison,type)"
                        + "VALUES ('"+data.getEtatSante()+"','"+data.getMotivation()+"','"+data.getDateSoumission()+"',"
                        + "'"+dateRejecte+"','"+dateValidation+"','"+raison+"','"+type+"')");
    }

    @Override
    public ResultRequest update(Candidature data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest delete(Candidature data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest fetchAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest fetchOne(Candidature data) {
        ResultRequest result=requete.fetchOne("SELECT * FROM candidature WHERE id='"+data.getId()+"'");
        if(result.getStatut()!=ResultRequest.OK) return null;
        if(result.getStatut()!=ResultRequest.OK  || result.getResult()==null) return null;
        Map resultData=(Map) result.getResult();
        int type=parseInt(resultData.get("type").toString());
        switch (type) {
            case 0:
                result.setResult(new Candidature(parseInt(resultData.get("id").toString()),
                        (String)resultData.get("date_soumission"),
                        (String)resultData.get("etat_sante"),
                        (String)resultData.get("motivation")));
                break;
            case 1:
                result.setResult(new CandidatureValide(parseInt(resultData.get("id").toString()),
                        (String)resultData.get("date_soumission"),
                        (String)resultData.get("date_validation"),
                        (String)resultData.get("etat_sante"),
                        (String)resultData.get("motivation")));
                break;
            case 2:
                result.setResult(new CandidatureRejecte(parseInt(resultData.get("id").toString()),
                        (String)resultData.get("date_soumission"),
                        (String)resultData.get("date_rejet"),
                        (String)resultData.get("etat_sante"),
                        (String)resultData.get("motivation"),
                        (String)resultData.get("raison")));
                break;
            default:
                result.setResult(null);
                break;
        }
        return result;
    }

   
    
}

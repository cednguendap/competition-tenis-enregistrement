/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.databaseaccess;

import java.util.ArrayList;

/**
 *
 * @author NGUENDAP
 */
public class ResultRequest {
    private String action;
    private Object result;
    private int statut;
    private String descriptionResult;    
    public static int ERROR=-1;
    public static int OK=0;
    

    public ResultRequest(String action, Object result, String descriptionResult,int statut) {
        this.action = action;
        this.result = result;
        this.descriptionResult = descriptionResult;
        this.statut=statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getStatut() {
        return statut;
    }

    public String getAction() {
        return action;
    }

    public Object getResult() {
        return result;
    }

    public String getDescriptionResult() {
        return descriptionResult;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public void setDescriptionResult(String descriptionResult) {
        this.descriptionResult = descriptionResult;
    }
    
    
    
}

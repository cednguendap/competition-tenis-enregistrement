/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.databaseaccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NGUENDAP
 */
public class Requete {
    private Connection connexionObject;
    private Statement statement=null;
    ResultSet resultSet=null; 
    
    public Requete(Connection con)
    {
        this.connexionObject=con;
    }
    
    public ResultRequest add(String requestString)
    {        
        ResultRequest result=new ResultRequest("Add object ",null,"",ResultRequest.OK);
        execRequest(requestString);
        /*if(resultSet==null)
        {
            result.setStatut(ResultRequest.ERROR);
            result.setDescriptionResult("An erro Occur");
        }     
        else
        {
            
        }*/
        closeResultSet();
        return result;
    }
    public ResultRequest fetchAll(String requestString)
    {
        ResultRequest result=new ResultRequest("Fetch All Objects ",null,"",ResultRequest.OK);
        execRequest(requestString);
        ResultSetMetaData resultSetMetaData=null;
        ArrayList<Map> resultData=new ArrayList<>();
        try {            
            resultSetMetaData = resultSet.getMetaData();
            while(resultSet.next())
            {  
                Map mapData=new HashMap();
                for(int i = 1; i <= resultSetMetaData.getColumnCount(); i++)
                {
                    if(resultSet.getObject(i)!=null) mapData.put(resultSetMetaData.getColumnName(i).toLowerCase(),resultSet.getObject(i).toString());
                }
                resultData.add(mapData);
            }
            result.setResult(resultData);
            closeResultSet();
        } catch (SQLException ex) {
            Logger.getLogger(Requete.class.getName()).log(Level.SEVERE, null, ex);
            result.setStatut(ResultRequest.ERROR);
            result.setDescriptionResult(ex.getMessage());
        }
        
        return result;
    }
    public ResultRequest fetchOne(String requestString)
    {
        ResultRequest result=null;        
        result=fetchAll(requestString);
        if(((ArrayList)result.getResult()).size()>0) result.setResult(((ArrayList)result.getResult()).get(0));        
        else result.setResult(null);
        return result;
    }
    public ResultRequest update(String requestString)
    {
        ResultRequest result=null;
        execRequest(requestString);
        return result;
    }
    public ResultRequest delete(String requestString)
    {
        ResultRequest result=null;
        //execRequest();
        return result;
    }
    private ResultSet execRequest(String requeteString)
    {                
        try {
            statement=connexionObject.createStatement();
            if(statement.execute(requeteString))
            {
                resultSet=statement.getResultSet();                
            }
        } catch (Exception ex) {
            Logger.getLogger(Requete.class.getName()).log(Level.SEVERE, null, ex);
        }     
        return resultSet;
    }
    private void closeResultSet()
    {
        if (statement != null) 
        {
            try {
                statement.close();
            } catch (SQLException sqlEx) { } // ignore

            statement = null;
        }
        if (resultSet != null) 
        {
            try {
                resultSet.close();
            } catch (SQLException sqlEx) { } // ignore

            resultSet = null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis.databaseaccess;

import competitiontenis.interfaces.DAOController;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import model.Administrateur;
import model.Candidature;
import model.Participant;
import model.User;

/**
 *
 * @author NGUENDAP
 */
public class DAOUserController extends DAOController<User>
{
    private DAOController<Candidature> daoCandidature;
    public DAOUserController(DAOController<Candidature> daoCandidature)
    {
           this.daoCandidature=daoCandidature;
    }
    public User connexion(String email,String password)
    {
        System.out.println(requete.toString());
        ResultRequest result=requete.fetchOne("SELECT * FROM participant WHERE email='"+email+"' AND mdp='"+password+"'");
        if(result.getStatut()!=ResultRequest.OK) return null;
        if(result.getResult()==null)
        {
            System.out.println("Dans l'administrateur");
            result=requete.fetchOne("SELECT * FROM administrateur WHERE email='"+email+"' AND mdp='"+password+"'");
            if(result.getStatut()!=ResultRequest.OK  || result.getResult()==null) return null;
            Map resultData=(Map) result.getResult();
            Administrateur admin= new Administrateur(parseInt(resultData.get("id").toString()),
                (String)resultData.get("nom"),
                (String)resultData.get("prenom"),
                (String)resultData.get("tel"),
                (String)resultData.get("email"),
                (String)resultData.get("mdp"),
                (String)resultData.get("sexe"),
                (String)resultData.get("photo"));  
            return admin;
        }
        Map resultData=(Map) result.getResult();
        return new Participant(parseInt(resultData.get("id").toString()),
                (String)resultData.get("nom"),
                (String)resultData.get("prenom"),
                (String)resultData.get("tel"),
                (String)resultData.get("email"),
                (String)resultData.get("mdp"),
                (String)resultData.get("sexe"),
                (String)resultData.get("photo"));  
    }

    @Override
    public ResultRequest save(User data) {
        return requete.add("INSERT INTO participant (nom,prenom,photo,email,tel,sexe,mdp)"
                + "VALUES ('"+data.getNom()+"','"+data.getPrenom()+"','"+data.getPhoto()+"','"+data.getEmail()
                +"','"+data.getTel()+"','"+data.getSexe()+"','"+data.getMdp()+"')");
    }

    @Override
    public ResultRequest update(User data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest delete(User data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultRequest fetchAll() {
        ResultRequest result=requete.fetchAll("SELECT * FROM participant ");
           if(result.getStatut()!=ResultRequest.OK  || result.getResult()==null) return null;
           ArrayList<Map> resultData=(ArrayList<Map>) result.getResult();
           ArrayList<Participant> participants=new ArrayList<>();
           for(Map participant:resultData)
           {
               Participant newParticipant= new Participant(parseInt(participant.get("id").toString()),
               (String)participant.get("nom"),
               (String)participant.get("prenom"),
               (String)participant.get("tel"),
               (String)participant.get("email"),
               (String)participant.get("mdp"),
               (String)participant.get("sexe"),
               (String)participant.get("photo"));
               if(participant.containsKey("candidature_id"))
               {
                   ResultRequest resultCandidature=daoCandidature.fetchOne(new Candidature(parseInt(participant.get("candidature_id").toString()),"","",""));
                   if(resultCandidature.getStatut()==ResultRequest.OK) newParticipant.setCandidature((Candidature)resultCandidature.getResult());
               }
               participants.add(newParticipant);
           }
           result.setResult(participants);
           return result;
    }

    @Override
    public ResultRequest fetchOne(User data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

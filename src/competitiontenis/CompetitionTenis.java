/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package competitiontenis;

import competitiontenis.auth.AuthController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author NGUENDAP
 */
public class CompetitionTenis extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        //stage.initStyle(StageStyle.TRANSPARENT);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(CompetitionTenis.class.getResource("auth/auth.fxml"));
        AuthController authController=new  AuthController(stage);
        loader.setController(authController);
        BorderPane root = (BorderPane)loader.load();
        authController.setRootLayout(root);
        authController.showSignupUI();
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

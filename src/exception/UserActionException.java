/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

import model.User;

/**
 *
 * @author NGUENDAP
 */
public class UserActionException  extends RuntimeException{
    private User user;
    private String message;

    public UserActionException(User user, String message) {
        super(message);
        this.user = user;
        this.message = message;
    }

    public UserActionException(User user) {
        this.user = user;
        this.message = "invalid action";
    }
    @Override
    public String toString() {
        return "UserActionException{" + "user=" + user + ", message=" + message + '}';
    }
    
}

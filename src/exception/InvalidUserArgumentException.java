/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

import model.User;

/**
 *
 * @author NGUENDAP
 */
public class InvalidUserArgumentException extends UserActionException
{

    public InvalidUserArgumentException(User user, String message) {
        super(user, message);
    }
    public InvalidUserArgumentException(User user)
    {
        super(user,"Invalid Argument Exception");
    }
    
}

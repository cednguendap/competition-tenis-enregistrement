/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

import model.Candidature;

/**
 *
 * @author NGUENDAP
 */
public class CandidatureException extends RuntimeException
{
    private Candidature candidature;
    private String message;

    public CandidatureException(Candidature candidature, String message) {
        super(message);
        this.candidature = candidature;
        this.message = message;
    }

    @Override
    public String toString() {
        return "CandidatureException{" + "candidature=" + candidature + ", message=" + message + '}';
    }
    
}
